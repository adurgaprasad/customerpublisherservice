package com.prokarma.engineering.customer.publisher.controller;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.bind.MissingRequestHeaderException;

import com.prokarma.engineering.customer.publisher.convert.KafkaRequestConverterImpl;
import com.prokarma.engineering.customer.publisher.domain.CustomerAddress;
import com.prokarma.engineering.customer.publisher.domain.CustomerServiceRequest;
import com.prokarma.engineering.customer.publisher.domain.CustomerServiceRequest.CustomerStatusEnum;
import com.prokarma.engineering.customer.publisher.service.CustomerPublisherServiceImpl;
import com.prokarma.engineering.customer.publisher.util.ObjectMapperUtil;

@WebMvcTest(CustomerPublisherController.class)
class CustomerPublisherControllerTest {
  @Autowired private MockMvc mockMvc;

  @MockBean private CustomerPublisherServiceImpl service;

  @MockBean private KafkaRequestConverterImpl kafkaConverter;

  @Test
  void testPublishCustomerDataWhenPassingValidRequestData() throws Exception {

    mockMvc
        .perform(
            post("/publisher/create-customer")
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(ObjectMapperUtil.convertObjectToJson(getCustomerData()))
                .headers(getHttpHeaders()))
        .andExpect(status().isOk());
  }

  @Test
  void testPublishCustomerDataWhenPassingInvalidHeader() throws Exception {

    mockMvc
        .perform(
            post("/publisher/create-customer")
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(ObjectMapperUtil.convertObjectToJson(getCustomerData()))
                .headers(getHttpHeadersWithMissingAuthorizationParameter()))
        .andExpect(status().is(400))
        .andExpect(
            result ->
                assertTrue(result.getResolvedException() instanceof MissingRequestHeaderException));
  }

  @Test
  void testPublishCustomerDataWhenPassingRequestHeaderWithMissingAuthorizationParameter()
      throws Exception {

    mockMvc
        .perform(
            post("/publisher/create-customer")
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(
                    ObjectMapperUtil.convertObjectToJson(getCustomerDataWithCustomerNumberNull()))
                .headers(getHttpHeadersWithMissingAuthorizationParameter()))
        .andExpect(status().is(400))
        .andExpect(
            result ->
                assertTrue(
                    result.getResolvedException()
                        instanceof org.springframework.web.bind.MethodArgumentNotValidException));
  }

  @Test
  void testPublishCustomerDataWhenPassingRequestBodyWithInvalidMoblileNumber() throws Exception {

    mockMvc
        .perform(
            post("/publisher/create-customer")
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(
                    ObjectMapperUtil.convertObjectToJson(getCustomerDataWithInvalidMoblieNumber()))
                .headers(getHttpHeadersWithMissingAuthorizationParameter()))
        .andExpect(status().is(400))
        .andExpect(
            result ->
                assertTrue(
                    result.getResolvedException()
                        instanceof org.springframework.web.bind.MethodArgumentNotValidException));
  }

  @Test
  void testSaveCustomerDataWhenWrongUrlPrivideThenReturn404StatusCode() throws Exception {
    mockMvc
        .perform(
            post("/publisher/create-cust")
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(ObjectMapperUtil.convertObjectToJson(getCustomerData()))
                .headers(getHttpHeaders()))
        .andExpect(status().is(404));
  }

  private CustomerServiceRequest getCustomerData() {

    CustomerServiceRequest customerRequest = new CustomerServiceRequest();
    customerRequest.setCustomerNumber("C000000001");
    customerRequest.setFirstName("durgaprasad");
    customerRequest.setLastName("durgaprasad");
    customerRequest.setBirthdate("27-11-1994");
    customerRequest.setCountry("india");
    customerRequest.setCountryCode("IN");
    customerRequest.setMobileNumber("5555551216");
    customerRequest.setEmail("abcdefg@gmail.com");
    customerRequest.setCustomerStatus(CustomerStatusEnum.RESTORED);

    CustomerAddress address = new CustomerAddress();
    address.setAddressLine1("string");
    address.setAddressLine2("string");
    address.setStreet("string");
    address.setPostalCode(12345L);

    customerRequest.setAddress(address);

    return customerRequest;
  }

  private CustomerServiceRequest getCustomerDataWithCustomerNumberNull() {
    CustomerServiceRequest request = getCustomerData();
    request.setCustomerNumber(null);
    return request;
  }

  private CustomerServiceRequest getCustomerDataWithInvalidMoblieNumber() {
    CustomerServiceRequest request = getCustomerData();
    request.setMobileNumber("55555512166");
    return request;
  }

  private HttpHeaders getHttpHeaders() {
    HttpHeaders headers = new HttpHeaders();
    headers.set("Content-Type", "application/json");
    headers.set("Authorization", "bearer 98358ce2-ff3c-4b82-8b10-bc7f4ab852e0");
    headers.set("Activity-Id", "123");
    headers.set("Transaction-Id", "producer-application");
    return headers;
  }

  private HttpHeaders getHttpHeadersWithMissingAuthorizationParameter() {
    HttpHeaders headers = new HttpHeaders();
    headers.set("Content-Type", "application/json");
    headers.set("Authorizatio", "bearer 98358ce2-ff3c-4b82-8b10-bc7f4ab852e0");
    headers.set("Activity-Id", "123");
    headers.set("Transaction-Id", "producer-application");
    return headers;
  }
}
