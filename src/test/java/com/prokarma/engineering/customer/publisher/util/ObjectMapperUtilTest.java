package com.prokarma.engineering.customer.publisher.util;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.Rule;
import org.junit.jupiter.api.Test;
import org.junit.rules.ExpectedException;

import com.prokarma.engineering.customer.publisher.domain.CustomerAddress;
import com.prokarma.engineering.customer.publisher.domain.CustomerServiceRequest;
import com.prokarma.engineering.customer.publisher.domain.CustomerServiceRequest.CustomerStatusEnum;

class ObjectMapperUtilTest {

  @Rule public ExpectedException expectedEx = ExpectedException.none();

  @Test
  void testConvertObjectToJson() {
    String expectedResult =
        "{\"customerNumber\":\"C000000001\",\"firstName\":\"durgaprasad\",\"lastName\":\"durgaprasad\",\"birthdate\":\"27-11-1994\",\"country\":\"india\",\"countryCode\":\"IN\",\"mobileNumber\":\"5555551216\",\"email\":\"abcdefg@gmail.com\",\"customerStatus\":\"Restored\",\"address\":{\"addressLine1\":\"string\",\"addressLine2\":\"string\",\"street\":\"string\",\"postalCode\":12345}}";

    String result = ObjectMapperUtil.convertObjectToJson(getCustomerData());
    assertEquals(expectedResult, result);
  }


  private CustomerServiceRequest getCustomerData() {

    CustomerServiceRequest customerRequest = new CustomerServiceRequest();
    customerRequest.setCustomerNumber("C000000001");
    customerRequest.setFirstName("durgaprasad");
    customerRequest.setLastName("durgaprasad");
    customerRequest.setBirthdate("27-11-1994");
    customerRequest.setCountry("india");
    customerRequest.setCountryCode("IN");
    customerRequest.setMobileNumber("5555551216");
    customerRequest.setEmail("abcdefg@gmail.com");
    customerRequest.setCustomerStatus(CustomerStatusEnum.RESTORED);

    CustomerAddress address = new CustomerAddress();
    address.setAddressLine1("string");
    address.setAddressLine2("string");
    address.setStreet("string");
    address.setPostalCode(12345L);

    customerRequest.setAddress(address);

    return customerRequest;
  }
}
