package com.prokarma.engineering.customer.publisher.service;

import static org.junit.Assert.assertEquals;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;

import com.prokarma.engineering.customer.publisher.domain.CustomerAddress;
import com.prokarma.engineering.customer.publisher.domain.CustomerServiceResponse;
import com.prokarma.engineering.customer.publisher.domain.KafkaRequestPayload;
import com.prokarma.engineering.customer.publisher.util.ObjectMapperUtil;

@RunWith(SpringRunner.class)
@DirtiesContext
@SpringBootTest()
class CustomerPublisherServiceImplTest {

  @Autowired private CustomerPublisherServiceImpl customerPublisherServiceImpl;

  @Test
  void testPublishCustomerRequest() {
    KafkaRequestPayload kafkaPayload = getKafkaPayload();
    CustomerServiceResponse response =
        customerPublisherServiceImpl.publishCustomerRequest(kafkaPayload);

    assertEquals("success", response.getStatus().toString());
  }

  private KafkaRequestPayload getKafkaPayload() {

    KafkaRequestPayload customerRequest = new KafkaRequestPayload();
    customerRequest.setCustomerNumber("C000000001");
    customerRequest.setFirstName("durgaprasad");
    customerRequest.setLastName("durgaprasad");
    customerRequest.setBirthdate("27-11-1994");
    customerRequest.setCountry("india");
    customerRequest.setCountryCode("IN");
    customerRequest.setMobileNumber("5555551216");
    customerRequest.setEmail("abcdefg@gmail.com");
    customerRequest.setCustomerStatus("RESTORED");

    CustomerAddress address = new CustomerAddress();
    address.setAddressLine1("string");
    address.setAddressLine2("string");
    address.setStreet("string");
    address.setPostalCode(12345L);

    customerRequest.setAddress(ObjectMapperUtil.convertObjectToJson(address));

    return customerRequest;
  }
}
