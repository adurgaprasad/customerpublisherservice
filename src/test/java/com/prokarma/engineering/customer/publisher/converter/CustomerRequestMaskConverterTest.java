package com.prokarma.engineering.customer.publisher.converter;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.prokarma.engineering.customer.publisher.convert.CustomerRequestMaskConverter;
import com.prokarma.engineering.customer.publisher.domain.CustomerAddress;
import com.prokarma.engineering.customer.publisher.domain.CustomerServiceRequest;
import com.prokarma.engineering.customer.publisher.domain.CustomerServiceRequest.CustomerStatusEnum;

class CustomerRequestMaskConverterTest {
	private CustomerRequestMaskConverter converter = new CustomerRequestMaskConverter();
	@Test
	  void testConvert() {

	    CustomerServiceRequest maskRequest = converter.convert(getCustomerData());
	    assertEquals("******0001", maskRequest.getCustomerNumber());
	    assertEquals("abc****@gmail.com", maskRequest.getEmail());
	    assertEquals("**-**-1994", maskRequest.getBirthdate());
	  }
	
	private CustomerServiceRequest getCustomerData() {

	    CustomerServiceRequest customerRequest = new CustomerServiceRequest();
	    customerRequest.setCustomerNumber("C000000001");
	    customerRequest.setFirstName("durgaprasad");
	    customerRequest.setLastName("durgaprasad");
	    customerRequest.setBirthdate("27-11-1994");
	    customerRequest.setCountry("india");
	    customerRequest.setCountryCode("IN");
	    customerRequest.setMobileNumber("5555551216");
	    customerRequest.setEmail("abcdefg@gmail.com");
	    customerRequest.setCustomerStatus(CustomerStatusEnum.RESTORED);

	    CustomerAddress address = new CustomerAddress();
	    address.setAddressLine1("string");
	    address.setAddressLine2("string");
	    address.setStreet("string");
	    address.setPostalCode(12345L);

	    customerRequest.setAddress(address);

	    return customerRequest;
	  }
}
