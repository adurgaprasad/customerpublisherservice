package com.prokarma.engineering.customer.publisher.controller;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.prokarma.engineering.customer.publisher.convert.CustomerRequestMaskConverter;
import com.prokarma.engineering.customer.publisher.convert.KafkaRequestConverterImpl;
import com.prokarma.engineering.customer.publisher.domain.CustomerServiceRequest;
import com.prokarma.engineering.customer.publisher.domain.CustomerServiceResponse;
import com.prokarma.engineering.customer.publisher.service.CustomerPublisherServiceImpl;

@RestController
@RequestMapping(value = "/publisher")
public class CustomerPublisherController {

  Logger logger = LoggerFactory.getLogger(CustomerPublisherController.class);

  @Autowired private KafkaRequestConverterImpl kafkaRequestConverter;

  @Autowired private CustomerRequestMaskConverter maskConverter;

  @Autowired private CustomerPublisherServiceImpl publisherService;

  @PostMapping(
      value = "/create-customer",
      produces = MediaType.APPLICATION_JSON_VALUE,
      consumes = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<CustomerServiceResponse> publishCustomerData(
      @RequestBody @Valid CustomerServiceRequest request,
      @RequestHeader(value = "Authorization", required = true) String authorization,
      @RequestHeader(value = "Activity-Id", required = true) String activityId,
      @RequestHeader(value = "Transaction-Id", required = true) String transactionId) {

    logger.info(
        "Customer request with activityId : {} is {}", activityId, maskConverter.convert(request));

    CustomerServiceResponse response =
        publisherService.publishCustomerRequest(
            kafkaRequestConverter.convert(request, activityId, transactionId));

    logger.info(
        "Response for the customer request with activityId : {}. is {}.", activityId, response);
    return new ResponseEntity<>(response, HttpStatus.OK);
  }
}
