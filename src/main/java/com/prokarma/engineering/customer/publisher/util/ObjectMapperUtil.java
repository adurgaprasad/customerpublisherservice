package com.prokarma.engineering.customer.publisher.util;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.prokarma.engineering.customer.publisher.exception.ProducerApplicationFailureException;

public class ObjectMapperUtil {

  private ObjectMapperUtil() {
    throw new AssertionError("No Object Creation");
  }

  public static String convertObjectToJson(Object object) {
    try {
      ObjectMapper objectMapper = new ObjectMapper();
      return objectMapper.writeValueAsString(object);
    } catch (JsonProcessingException ex) {
      throw new ProducerApplicationFailureException("Json processing exception" + ex);
    }
  }
}
