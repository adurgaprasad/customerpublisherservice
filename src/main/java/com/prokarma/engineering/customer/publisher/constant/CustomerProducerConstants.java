package com.prokarma.engineering.customer.publisher.constant;

public class CustomerProducerConstants {
  public static final String BIRTHDATE_REGEX = "\\\\d(?=(?:\\\\D*\\\\d){4})";
  public static final String CUSTOMERNUMBER_REGEX = "\\\\w(?=\\\\w{4})";
  public static final String EMAIL_REGEX = "(?<=.{3}).(?=.*@)";
  public static final String CLIENT = "client";
  public static final String CLIENT_PASSWORD = "password";
  public static final String PASSWORD_GRANT_TYPE = "password";
  public static final String READ_SCOPE = "read";
  public static final String WRITE_SCOPE = "write";

  public static String getClient() {
    return CLIENT;
  }

  public static String getClientPassword() {
    return CLIENT_PASSWORD;
  }

  public static String getPasswordGrantType() {
    return PASSWORD_GRANT_TYPE;
  }

  public static String getReadScope() {
    return READ_SCOPE;
  }

  public static String getWriteScope() {
    return WRITE_SCOPE;
  }

  public static String getBirthdateRegex() {
    return BIRTHDATE_REGEX;
  }

  public static String getCustomernumberRegex() {
    return CUSTOMERNUMBER_REGEX;
  }

  public static String getEmailRegex() {
    return EMAIL_REGEX;
  }

  private CustomerProducerConstants() {}
}
