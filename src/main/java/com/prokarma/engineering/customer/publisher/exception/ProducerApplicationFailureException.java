package com.prokarma.engineering.customer.publisher.exception;

public class ProducerApplicationFailureException extends RuntimeException {

  /** */
  private static final long serialVersionUID = 1L;

  public ProducerApplicationFailureException(Throwable ex) {
    super(ex);
  }

  public ProducerApplicationFailureException(String message) {
    super(message);
  }
}
