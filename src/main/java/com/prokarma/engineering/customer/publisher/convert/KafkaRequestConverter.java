package com.prokarma.engineering.customer.publisher.convert;

import com.prokarma.engineering.customer.publisher.domain.CustomerServiceRequest;
import com.prokarma.engineering.customer.publisher.domain.KafkaRequestPayload;

public interface KafkaRequestConverter {
	public KafkaRequestPayload convert(CustomerServiceRequest request, String activityId, String transactionId);
	}
