package com.prokarma.engineering.customer.publisher.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.SendResult;
import org.springframework.stereotype.Service;
import org.springframework.util.concurrent.ListenableFuture;
import org.springframework.util.concurrent.ListenableFutureCallback;

import com.prokarma.engineering.customer.publisher.domain.CustomerServiceResponse;
import com.prokarma.engineering.customer.publisher.domain.CustomerServiceResponse.StatusEnum;
import com.prokarma.engineering.customer.publisher.domain.KafkaRequestPayload;
import com.prokarma.engineering.customer.publisher.exception.ProducerApplicationFailureException;

@Service
public class CustomerPublisherServiceImpl implements CustomerPublisherService {

  Logger logger = LoggerFactory.getLogger(CustomerPublisherServiceImpl.class);

  @Autowired KafkaTemplate<String, KafkaRequestPayload> kafkaTemplate;

  @Value("${karafka.topic}")
  private String topic;

  @Override
  public CustomerServiceResponse publishCustomerRequest(KafkaRequestPayload customerRequest) {
    CustomerServiceResponse response = new CustomerServiceResponse();

    ListenableFuture<SendResult<String, KafkaRequestPayload>> future =
        kafkaTemplate.send(topic, customerRequest);

    future.addCallback(
        new ListenableFutureCallback<SendResult<String, KafkaRequestPayload>>() {
          @Override
          public void onFailure(Throwable ex) {
            logger.error("Error occured while publishing payload to the kafka topic :", ex);
            throw new ProducerApplicationFailureException(ex);
          }

          @Override
          public void onSuccess(SendResult<String, KafkaRequestPayload> result) {
        	  logger.info("Customer Info Published Successfully");
          }
        });
    StatusEnum success = StatusEnum.SUCCESS;
    response.setStatus(success);
    response.setMessage("Customer Data Successfully published");
    return response;
  }
}
