package com.prokarma.engineering.customer.publisher.exception;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.oauth2.common.exceptions.InvalidTokenException;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingRequestHeaderException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.prokarma.engineering.customer.publisher.domain.CustomerServiceResponse;
import com.prokarma.engineering.customer.publisher.domain.CustomerServiceResponse.StatusEnum;

@ControllerAdvice
public class CustomerPublisherControllerAdvice {

  @ExceptionHandler(ProducerApplicationFailureException.class)
  public ResponseEntity<CustomerServiceResponse> applicationException(
      ProducerApplicationFailureException ex, HttpServletRequest request) {
    CustomerServiceResponse errorResponse = new CustomerServiceResponse();
    StatusEnum failed = StatusEnum.FAILED;
    errorResponse.setStatus(failed);
    errorResponse.setMessage(ex.getMessage());
    errorResponse.setErrorType(ex.getClass().getSimpleName());

    return new ResponseEntity<>(errorResponse, HttpStatus.INTERNAL_SERVER_ERROR);
  }

  @ExceptionHandler({MethodArgumentNotValidException.class, MissingRequestHeaderException.class})
  public ResponseEntity<CustomerServiceResponse> methodArgumentNotValidHandler(
      MethodArgumentNotValidException ex, HttpServletRequest request) {
    CustomerServiceResponse errorResponse = new CustomerServiceResponse();
    StatusEnum failed = StatusEnum.FAILED;
    errorResponse.setStatus(failed);
    errorResponse.setMessage(ex.getMessage());
    errorResponse.setErrorType(ex.getClass().getSimpleName());
    return new ResponseEntity<>(errorResponse, HttpStatus.BAD_REQUEST);
  }

  @ExceptionHandler({InvalidTokenException.class, AuthenticationException.class})
  @ResponseStatus(value = HttpStatus.UNAUTHORIZED)
  @ResponseBody
  public ResponseEntity<CustomerServiceResponse> handleAuthenticationException(Exception ex) {

    CustomerServiceResponse errorResponse = new CustomerServiceResponse();
    StatusEnum failed = StatusEnum.FAILED;
    errorResponse.setStatus(failed);
    errorResponse.setMessage(ex.getMessage());
    errorResponse.setErrorType(ex.getClass().getSimpleName());
    return new ResponseEntity<>(errorResponse, HttpStatus.UNAUTHORIZED);
  }
}
