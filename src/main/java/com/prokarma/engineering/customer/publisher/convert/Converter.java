package com.prokarma.engineering.customer.publisher.convert;

import com.prokarma.engineering.customer.publisher.domain.CustomerServiceRequest;

public interface Converter {
	public CustomerServiceRequest convert(CustomerServiceRequest request);
}
