package com.prokarma.engineering.customer.publisher.service;

import com.prokarma.engineering.customer.publisher.domain.CustomerServiceResponse;
import com.prokarma.engineering.customer.publisher.domain.KafkaRequestPayload;

public interface CustomerPublisherService {
  public CustomerServiceResponse publishCustomerRequest(KafkaRequestPayload kafkaRequest);
}
