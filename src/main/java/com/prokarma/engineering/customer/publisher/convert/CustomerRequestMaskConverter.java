package com.prokarma.engineering.customer.publisher.convert;

import org.springframework.stereotype.Component;

import com.prokarma.engineering.customer.publisher.constant.CustomerProducerConstants;
import com.prokarma.engineering.customer.publisher.domain.CustomerServiceRequest;

@Component
public class CustomerRequestMaskConverter implements Converter {

  @Override
  public CustomerServiceRequest convert(CustomerServiceRequest request) {
    CustomerServiceRequest maskCustomerRequest = new CustomerServiceRequest();
    maskCustomerRequest.setCustomerNumber(
        request
            .getCustomerNumber()
            .replaceAll(CustomerProducerConstants.CUSTOMERNUMBER_REGEX, "*"));
    maskCustomerRequest.setFirstName(request.getFirstName());
    maskCustomerRequest.setLastName(request.getLastName());
    maskCustomerRequest.setBirthdate(
        request.getBirthdate().replaceAll(CustomerProducerConstants.BIRTHDATE_REGEX, "*"));
    maskCustomerRequest.setCountry(request.getCountry());
    maskCustomerRequest.setCountryCode(request.getCountryCode());
    maskCustomerRequest.setMobileNumber(request.getMobileNumber());
    maskCustomerRequest.setEmail(
        request.getEmail().replaceAll(CustomerProducerConstants.EMAIL_REGEX, "*"));
    maskCustomerRequest.setCustomerStatus(request.getCustomerStatus());
    maskCustomerRequest.setAddress(request.getAddress());

    return maskCustomerRequest;
  }
}
