package com.prokarma.engineering.customer.publisher.convert;

import org.springframework.stereotype.Component;

import com.prokarma.engineering.customer.publisher.domain.CustomerServiceRequest;
import com.prokarma.engineering.customer.publisher.domain.KafkaRequestPayload;
import com.prokarma.engineering.customer.publisher.util.ObjectMapperUtil;

@Component
public class KafkaRequestConverterImpl implements KafkaRequestConverter {

  @Override
  public KafkaRequestPayload convert(
      CustomerServiceRequest request, String activityId, String transactionId) {
    KafkaRequestPayload kafkaRequest = new KafkaRequestPayload();
    kafkaRequest.setCustomerNumber(request.getCustomerNumber());
    kafkaRequest.setFirstName(request.getFirstName());
    kafkaRequest.setLastName(request.getLastName());
    kafkaRequest.setBirthdate(request.getBirthdate());
    kafkaRequest.setCountry(request.getCountry());
    kafkaRequest.setCountryCode(request.getCountryCode());
    kafkaRequest.setMobileNumber(request.getMobileNumber());
    kafkaRequest.setEmail(request.getEmail());
    kafkaRequest.setCustomerStatus(request.getCustomerStatus().toString());
    kafkaRequest.setAddress(ObjectMapperUtil.convertObjectToJson(request.getAddress()));
    kafkaRequest.setActivityId(activityId);
    kafkaRequest.setTransactionId(transactionId);

    return kafkaRequest;
  }
}
